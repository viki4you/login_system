class AddKindToBooking < ActiveRecord::Migration[5.0]
  def change
    add_column :bookings, :from_kind, :string
    add_column :bookings, :to_kind, :string
  end
end
