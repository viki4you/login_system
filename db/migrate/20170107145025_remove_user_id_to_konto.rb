class RemoveUserIdToKonto < ActiveRecord::Migration[5.0]
  def change
    remove_column :kontos, :user_id, :integer
  end
end
