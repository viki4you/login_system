class RemoveKindFromBooking < ActiveRecord::Migration[5.0]
  def change
    remove_column :bookings, :kind, :string
  end
end
