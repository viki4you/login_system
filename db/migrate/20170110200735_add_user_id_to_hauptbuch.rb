class AddUserIdToHauptbuch < ActiveRecord::Migration[5.0]
  def change
    add_column :hauptbuches, :user_id, :integer
  end
end
