class CreateKontos < ActiveRecord::Migration[5.0]
  def change
    create_table :kontos do |t|
      t.string :soll
      t.string :soll_betrag
      t.string :haben
      t.string :haben_betrag

      t.timestamps
    end
  end
end
