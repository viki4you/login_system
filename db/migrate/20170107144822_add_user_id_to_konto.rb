class AddUserIdToKonto < ActiveRecord::Migration[5.0]
  def change
    add_column :kontos, :user_id, :integer
  end
end
