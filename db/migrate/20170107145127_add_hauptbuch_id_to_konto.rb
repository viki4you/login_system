class AddHauptbuchIdToKonto < ActiveRecord::Migration[5.0]
  def change
    add_column :kontos, :hauptbuch_id, :integer
  end
end
