class AddKontoNameToHauptbuch < ActiveRecord::Migration[5.0]
  def change
    add_column :hauptbuches, :konto_name, :string
  end
end
