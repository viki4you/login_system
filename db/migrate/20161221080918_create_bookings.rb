class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.string :from_account
      t.string :from_amount
      t.string :to_account
      t.string :to_amount
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :bookings, [:user_id, :created_at]
  end
end
