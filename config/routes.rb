Rails.application.routes.draw do
  resources :hauptbuches
  root   'static_pages#home'
  
  get 'bookings/new'
  get 'users/new'

  get 'konto/show'
  
  get  '/hauptbuch', to: 'hauptbuch#index'

  get    '/help',    to: 'static_pages#help'
  get    '/about',   to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  
  #resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :datens
  resources :bookings
  
  resources :users do
    member do
      get :bookings
    end
  end
  
  resources :users do
    member do
      get :hauptbuchs
    end
  end
end