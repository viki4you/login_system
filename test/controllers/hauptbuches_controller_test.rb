require 'test_helper'

class HauptbuchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hauptbuch = hauptbuches(:one)
  end

  #test "should get index" do
  #  get hauptbuches_url
  #  assert_response :success
  #end

  #test "should get new" do
  #   get new_hauptbuch_url
  #  assert_response :success
  #end

  #test "should create hauptbuch" do
  #  assert_difference('Hauptbuch.count') do
  #    post hauptbuches_url, params: { hauptbuch: {  } }
  #  end

  #  assert_redirected_to hauptbuch_url(Hauptbuch.last)
  #end

  test "should show hauptbuch" do
    get hauptbuch_url(@hauptbuch)
    assert_response :success
  end

  test "should get edit" do
    get edit_hauptbuch_url(@hauptbuch)
    assert_response :success
  end

  #test "should update hauptbuch" do
  #  patch hauptbuch_url(@hauptbuch), params: { hauptbuch: {  } }
  #  assert_redirected_to hauptbuch_url(@hauptbuch)
  #end

  test "should destroy hauptbuch" do
    assert_difference('Hauptbuch.count', -1) do
      delete hauptbuch_url(@hauptbuch)
    end

    assert_redirected_to hauptbuches_url
  end
  
  
end
