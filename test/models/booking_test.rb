require 'test_helper'

class BookingTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @booking = @user.bookings.build(from_kind: "n", 
                            from_account: "Bank",
                            from_amount: "10.5",
                            to_kind: "b",
                            to_account: "Kasse", 
                            to_amount: "10.6")
  end

  test "should be valid" do
    assert @booking.valid?
  end

  test "user id should be present" do
    @booking.user_id = nil
    assert_not @booking.valid?
  end

  test "user from_kind should be present" do
    @booking.from_kind = nil
    assert_not @booking.valid?
  end
  
  test "from_account should be present" do
    @booking.from_account = "   "
    assert_not @booking.valid?
  end
  
  test "from_amount should be present" do
    @booking.from_amount = "   "
    assert_not @booking.valid?
  end
  
  test "user to_kind should be present" do
    @booking.to_kind = nil
    assert_not @booking.valid?
  end
  
  test "to_account should be present" do
    @booking.to_account = "   "
    assert_not @booking.valid?
  end
  
  test "to_amount should be present" do
    @booking.to_amount = "   "
    assert_not @booking.valid?
  end

  test "from_account should be at most 250 characters" do
    @booking.from_account = "a" * 251
    assert_not @booking.valid?
  end
  
  test "from_amount should be at most 250 characters" do
    @booking.from_amount = "a" * 251
    assert_not @booking.valid?
  end
  
  test "to_account should be at most 250 characters" do
    @booking.to_account = "a" * 251
    assert_not @booking.valid?
  end
  
  test "to_amount should be at most 250 characters" do
    @booking.to_amount = "a" * 251
    assert_not @booking.valid?
  end
  
  test "order should be most recent first" do
    assert_equal bookings(:most_recent), Booking.first
  end
end
