json.array!(@hauptbuches) do |hauptbuch|
  json.extract! hauptbuch, :id
  json.url hauptbuch_url(hauptbuch, format: :json)
end
