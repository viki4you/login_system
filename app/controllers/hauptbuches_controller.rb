class HauptbuchesController < ApplicationController
  before_action :set_hauptbuch, only: [:show, :edit, :update, :destroy]

  # GET /hauptbuches
  # GET /hauptbuches.json
  def index
    @hauptbuches = current_user.hauptbuches.all
  end

  # GET /hauptbuches/1
  # GET /hauptbuches/1.json
  def show
  end

  # GET /hauptbuches/new
  def new
    @hauptbuch = Hauptbuch.new
  end

  # GET /hauptbuches/1/edit
  def edit
  end

  # POST /hauptbuches
  # POST /hauptbuches.json
  def create
    @hauptbuch = Hauptbuch.new(hauptbuch_params)

    respond_to do |format|
      if @hauptbuch.save
        format.html { redirect_to @hauptbuch, notice: 'Hauptbuch was successfully created.' }
        format.json { render :show, status: :created, location: @hauptbuch }
      else
        format.html { render :new }
        format.json { render json: @hauptbuch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hauptbuches/1
  # PATCH/PUT /hauptbuches/1.json
  def update
    respond_to do |format|
      if @hauptbuch.update(hauptbuch_params)
        format.html { redirect_to @hauptbuch, notice: 'Hauptbuch was successfully updated.' }
        format.json { render :show, status: :ok, location: @hauptbuch }
      else
        format.html { render :edit }
        format.json { render json: @hauptbuch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hauptbuches/1
  # DELETE /hauptbuches/1.json
  def destroy
    @hauptbuch.destroy
    respond_to do |format|
      format.html { redirect_to hauptbuches_url, notice: 'Hauptbuch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hauptbuch
      @hauptbuch = Hauptbuch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hauptbuch_params
      params.fetch(:hauptbuch, {})
    end
end
