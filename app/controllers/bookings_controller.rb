class BookingsController < ApplicationController
  before_action :logged_in_user, only: [:index, :new]

  def new
    @booking = current_user.bookings.new
  end
  
  def index
    @bookings = current_user.bookings.paginate(page: params[:page])
  end
  
  def create
    @hauptbuch = current_user.hauptbuches
    if !@hauptbuch.exists?(konto_name: booking_params[:from_account])
      @hauptbuch.create(konto_name: booking_params[:from_account])
    end
    
    @konto = current_user.kontos
    if booking_params[:from_kind] == "b" then
      @konto.create(name: booking_params[:from_account],
                    soll: "-",
                    soll_betrag: "-",
                    haben: booking_params[:to_account],
                    haben_betrag: booking_params[:to_amount])
    else
      @konto.create(name: booking_params[:from_account],
                    soll: booking_params[:to_account],
                    soll_betrag: booking_params[:to_amount],
                    haben: "-",
                    haben_betrag: "-")
    end
    
    if !@hauptbuch.exists?(konto_name: booking_params[:to_account])
      @hauptbuch.create(konto_name: booking_params[:to_account])
    end
    
    if booking_params[:to_kind] == "b" then
      @konto.create(name: booking_params[:to_account],
                    soll: booking_params[:from_account],
                    soll_betrag: booking_params[:from_amount],
                    haben: "-",
                    haben_betrag: "-")
    else
      @konto.create(name: booking_params[:to_account],
                    soll: "-",
                    soll_betrag: "-",
                    haben: booking_params[:from_account],
                    haben_betrag: booking_params[:from_amount])
    end
    
    flash[:info] = "In das Hauptbuch eingetragen, neuen Kontoeintrag erstellt."
    
    @booking = current_user.bookings.new(booking_params)
    if @booking.save
      #flash[:info] = "Buchung erfolgreich."
      redirect_to bookings_path
    else
      render 'new'
    end
  end
  
  private

    def booking_params
      params.require(:booking).permit(:from_kind, :from_account, :from_amount, 
                      :to_kind, :to_account, :to_amount)
    end
    
    def hauptbuch_params
      #params.require(:hauptbuch).permit(:konto_name)
    end
end
