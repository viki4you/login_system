require 'will_paginate/array'

class KontoController < ApplicationController
  def create
    
  end
  
  def show
    soll = Array.new
    haben = Array.new
    konto = Array.new
    konto_length = 0
    current_user.bookings.order('id ASC').reverse_order.each do |f|
      if f.from_account.to_s == params[:konto_name].to_s
        if f.from_kind.to_s == "b" then
          haben.push(:account => f.to_account, :amount => f.to_amount)
        else
          soll.push(:account => f.to_account, :amount => f.to_amount)
        end
      end
      if f.to_account.to_s == params[:konto_name].to_s
        if f.to_kind.to_s == "b" then
          soll.push(:account => f.from_account, :amount => f.from_amount)
        else
          haben.push(:account => f.from_account, :amount => f.from_amount)
        end
      end
    end
    if soll.size > haben.size then
      konto_length = soll.size - 1
    else
      konto_length = haben.size - 1
    end
    (0..konto_length).each do |i|
      if soll[i].nil?
        soll.push(:account => '-', :amount => '-')
      end
      if haben[i].nil?
        haben.push(:account => '-', :amount => '-')
      end
      
      konto.push(:id => i, 
                :from_account => soll[i][:account], 
                :from_amount => soll[i][:amount],
                :to_account => haben[i][:account],
                :to_amount => haben[i][:amount])
    end
    @konto = konto
    @konto = @konto.paginate(page: params[:page])
    
    #@konto = Konto.new(konto)
    #if @konto.save
    #  redirect_to root_url
    #else
    #  render 'new'
    #end
  end
end
