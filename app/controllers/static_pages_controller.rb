class StaticPagesController < ApplicationController

  def home
    if logged_in?
      #@micropost  = current_user.microposts.build
      @booking  = current_user.bookings.build
      #@feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end
  
  def about
  end

  def contact
  end
  
  def database
  end
end
