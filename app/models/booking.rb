class Booking < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :from_kind, presence: true, length: { maximum: 1 }
  validates :from_account, presence: true, length: { maximum: 250 }
  validates :from_amount, presence: true, length: { maximum: 250 }
  validates :to_kind, presence: true, length: { maximum: 1 }
  validates :to_account, presence: true, length: { maximum: 250 }
  validates :to_amount, presence: true, length: { maximum: 250 }
  default_scope -> { order(created_at: :desc) }
end
